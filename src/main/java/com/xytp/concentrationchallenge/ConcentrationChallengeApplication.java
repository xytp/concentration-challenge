package com.xytp.concentrationchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConcentrationChallengeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConcentrationChallengeApplication.class, args);
    }

}
